/*
 * PROGRAM:
 *   ChainBrewing - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   ChainBrewing is a Bukkit plugin designed to make a "brewing production chain".
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.chainbrewing;

import org.bukkit.block.BlockFace;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

class MiscFunc
{
	/// Returns the cardinally opposed face
	static public BlockFace opposedFace(BlockFace face)
	{
		switch(face)
		{
			case NORTH:
				return BlockFace.SOUTH;
			case SOUTH:
				return BlockFace.NORTH;
			
			case EAST:
				return BlockFace.WEST;
			case WEST:
				return BlockFace.EAST;
		}
		return BlockFace.SELF;
	}

	/// Returns a BlockFace linked to an <int> var. Useful for loops.
	static public BlockFace idToFace(int id)
	{
		switch(id)
		{
			case 0:
				return BlockFace.NORTH;
			case 1:
				return BlockFace.EAST;
			case 2:
				return BlockFace.SOUTH;
			case 3:
				return BlockFace.WEST;
		}
		return BlockFace.SELF;
	}

	/// Returns an <int> linked to a BlockFace. Useful for loops.
	static public int faceToId(BlockFace face)
	{
		switch(face)
		{
			case NORTH:
				return 0;
			case EAST:
				return 1;
			case SOUTH:
				return 2;
			case WEST:
				return 3;
		}
		return -1;
	}

	/// Returns true if the ItemStack contains anything
	static public boolean isEmpty(ItemStack stack)
	{
		if(stack == null || stack.getAmount() == 0 || stack.getType() == Material.AIR)
			return false;
		
		return true;
	}
}
