/*
 * PROGRAM:
 *   ChainBrewing - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   ChainBrewing is a Bukkit plugin designed to make a "brewing production chain".
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.chainbrewing;

import java.util.logging.Logger;
import java.util.ArrayList;
import org.bukkit.block.Block;
import org.bukkit.Material;
import org.bukkit.block.BrewingStand;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Location;

import fr.tobast.bukkit.chainbrewing.BrewingStandBlock;

class ScheduleWatcher implements Runnable
{
	private ArrayList<BrewingStandBlock> watchlist = new ArrayList<BrewingStandBlock>();
	private ArrayList<Integer> watchlist_del = new ArrayList<Integer>(); // Counts the times it has nothing to do. If more than 3, delete in the watchlist.
	Logger log;

	public ScheduleWatcher(Logger iLog)
	{
		log=iLog;
	}

	public int findWatchlist(BrewingStandBlock ref)
	{
		Location loc_ref=ref.block.getLocation();
		for(int i=0;i<watchlist.size();i++)
		{
			if(watchlist.get(i).block.getLocation().equals(loc_ref))
			{
				return i;
			}
		}
		return -1;
	}

	public boolean delItem(BrewingStandBlock ref)
	{
		int id=watchlist.indexOf(ref);
		if(id<0)
			return false;
		watchlist.remove(id);
		watchlist_del.remove(id);
		return true;
	}

	public boolean addItem(BrewingStandBlock ref)
	{
		watchlist_del.add(0);
		return watchlist.add(ref);
	}

	public void run()
	{
		for(int i=0;i<watchlist.size();i++)
		{
			BrewingStandBlock curr=watchlist.get(i);

			if(curr == null) // Somehow, the block was deleted
			{
				delItem(curr);
				continue;
			}


			if(curr.block==null || curr.block.getType()!=Material.BREWING_STAND)  // it was deleted from the world
			{
				delItem(curr);
				continue;
			}
			BrewingStandBlock reprBlock = curr;

			boolean hasChanges=false;
			boolean skipChain=false;

			while(curr.real)
			{
				if(!curr.block.getChunk().isLoaded()) // Unloaded
				{
					delItem(reprBlock);
					skipChain=true;
					break;
				}

				updateStand(curr);

				if(standHasChanges(curr))
					hasChanges=true;

				curr=curr.getReceiving();
			}
			if(skipChain)
				continue;

			if(!hasChanges && emptyUpChest(curr))
			{
				watchlist_del.set(i, ((int)watchlist_del.get(i))+1);
				if(((int)watchlist_del.get(i)) >= 3)
				{
					delItem(watchlist.get(i));
				}
			}
			else
			{
				watchlist_del.set(i, 0);
			}
		}
	}

	private boolean standHasChanges(BrewingStandBlock stand)
	{
		if(stand == null)
			return false;
		BrewingStand brewstand=((BrewingStand)stand.block.getState());
		if(brewstand == null)
			return false;

		if(brewstand.getBrewingTime() != 0)
			return true;
		
		if(emptyBrewingStand(brewstand))
			return false;
		return true;
	}

	private boolean emptyUpChest(BrewingStandBlock currStand)
	{
		while(currStand.real)
			currStand=currStand.getReceiving();

		if(!currStand.chest)
			return true;

		Inventory chestInv=((Chest)currStand.block.getState()).getInventory();
		int firstBottle=chestInv.first(Material.GLASS_BOTTLE);
		if(firstBottle<0)
		{
			firstBottle=chestInv.first(Material.POTION);
			if(firstBottle<0)
				return true;
		}

		return false;
	}

	private void updateStand(BrewingStandBlock stand)
	{
		if(stand == null)
			return;
		BrewingStand curr_stand=(BrewingStand)stand.block.getState();
		if(curr_stand == null)
			return;

		if(curr_stand.getBrewingTime() != 0) // it's brewing atm
			return;

		Inventory stand_inventory=curr_stand.getInventory();
		ItemStack stand_content[]=stand_inventory.getContents();

		if(!emptyBrewingStand(curr_stand))
		{
			// Push the items to the next element in chain
			BrewingStandBlock nextElem=stand.getSending();
			if(!nextElem.real)
			{
				if(nextElem.chest)
				{
					// It's a chest!
					Chest nextChest=((Chest)nextElem.block.getState());
					Inventory nextInv=nextChest.getInventory();
					if(nextInv==null)
						return;

					// Let's transfer items.
					for(int i=0;i<3;i++)
					{
						ItemStack caseContent=stand_content[i];
						int outId=nextInv.firstEmpty();
						if(outId<0)
							break;
						nextInv.setItem(outId, caseContent);
						stand_inventory.clear(i);
					}
				}
				else
					return;
			}
			else
			{
				BrewingStand nextStand=((BrewingStand)nextElem.block.getState());
				Inventory nextInv=nextStand.getInventory();
				if(nextInv==null)
					return;
				ItemStack nextContent[]=nextInv.getContents();

				if(!emptyBrewingStand(nextStand))
					return;

				// Let's charge the next one!
				for(int i=0;i<3;i++)
				{
					ItemStack caseContent=stand_content[i];
					nextInv.setItem(i, caseContent);
					stand_inventory.clear(i);
				}
			}
		}
		else
		{
			// Grab items from the previous element in chain
			BrewingStandBlock prevElem=stand.getReceiving();
			if(!prevElem.real)
			{
				if(prevElem.chest)
				{
					Chest prevChest=((Chest)prevElem.block.getState());
					Inventory prevInv=prevChest.getInventory();
					for(int i=0;i<3;i++)
					{
						int firstBottle=prevInv.first(Material.GLASS_BOTTLE);
						if(firstBottle<0)
						{
							firstBottle=prevInv.first(Material.POTION);
							if(firstBottle<0)
								break;
						}

						ItemStack currStack=prevInv.getItem(firstBottle);
						stand_inventory.setItem(i, currStack);
						prevInv.clear(firstBottle);
					}
				}
				else
					return;
			}
			else
			{
				BrewingStand prevStand=((BrewingStand)prevElem.block.getState());
				Inventory prevInv=prevStand.getInventory();
				if(prevInv==null)
					return;
				ItemStack prevContent[]=prevInv.getContents();

				if(emptyBrewingStand(prevStand))
					return;
				if(prevStand.getBrewingTime() != 0)
					return;

				// Let's charge the prev one!
				for(int i=0;i<3;i++)
				{
					ItemStack caseContent=prevContent[i];
					stand_inventory.setItem(i, caseContent);
					prevInv.clear(i);
				}
			}
		}
	}

	private boolean emptyBrewingStand(BrewingStand stand)
	{
		ItemStack stand_content[]=stand.getInventory().getContents();

		for(int i=0;i<3;i++)
		{
			if(stand_content[i]!=null)
				return false;
		}
		return true;
	}
}

