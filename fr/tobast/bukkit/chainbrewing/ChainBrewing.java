/*
 * PROGRAM:
 *   ChainBrewing - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   ChainBrewing is a Bukkit plugin designed to make a "brewing production chain".
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.chainbrewing;

// You just lost the game. Sorry.

import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;
import fr.tobast.bukkit.chainbrewing.PlacementListener;

public class ChainBrewing extends JavaPlugin
{
	Logger log=Logger.getLogger("Minecraft");

	public void onEnable()
	{
		getServer().getPluginManager().registerEvents(new PlacementListener(log, this), this);
		log.info("[ChainBrewing] Plugin enabled.");
	}

	public void onDisable()
	{
		log.info("[ChainBrewing] Plugin disabled.");
	}
}

