/*
 * PROGRAM:
 *   ChainBrewing - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   ChainBrewing is a Bukkit plugin designed to make a "brewing production chain".
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.chainbrewing;

import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.Material;

import fr.tobast.bukkit.chainbrewing.BrewingStandBlock;
import fr.tobast.bukkit.chainbrewing.MiscFunc;
import fr.tobast.bukkit.chainbrewing.ScheduleWatcher;

public class PlacementListener implements Listener
{
	Logger log;
	JavaPlugin plugin;
	ScheduleWatcher watcher;

	public PlacementListener(Logger iLog, JavaPlugin iPlugin)
	{
		log=iLog;
		plugin=iPlugin;
		watcher=new ScheduleWatcher(log);

		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, watcher, 100L, 50L);
	}

	/// handles events to check for a brewing stand interaction
	@EventHandler(priority=EventPriority.MONITOR)
		public void onPlayerInteractEvent(PlayerInteractEvent event)
		{
			Block clicked=event.getClickedBlock();
			if(clicked!=null && clicked.getType() == Material.BREWING_STAND) // Brewing stand
			{
				BrewingStandBlock brewingBlock=BrewingStandBlock.makeBrewingBlock(clicked);
				if(brewingBlock.hasRepeaters() && event.getPlayer().hasPermission("chainbrewing.chainbrew"))
					addBrewingChain(brewingBlock);
			}
		}

	private void addBrewingChain(BrewingStandBlock enterBlock)
	{
		BrewingStandBlock endStand=enterBlock;
		BrewingStandBlock currentStand=enterBlock.getSending();
		while(currentStand.real)
		{
			endStand=currentStand;
			currentStand=endStand.getSending();
		}

		//Here, endStand contains the 1st one.
		if(watcher.findWatchlist(endStand)<0)
			watcher.addItem(endStand);
	}
}

