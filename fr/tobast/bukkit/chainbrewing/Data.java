/*
 * PROGRAM:
 *   ChainBrewing - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   ChainBrewing is a Bukkit plugin designed to make a "brewing production chain".
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.chainbrewing;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.Material;

import fr.tobast.bukkit.chainbrewing.MiscFunc;

class BrewingStandBlock
{
	int repeaters[]={0,0,0,0}; // 0=no repeater ; 1 = receiving repeater ; 2 = sending repeater
	Block block;

	// Real represents a "real" brewing stand. Mean, if real is false, it's not a brewing stand.
	boolean real=true;
	boolean chest=false;


	public BrewingStandBlock()
	{
	}

	public BrewingStandBlock(boolean iReal)
	{
		// iReal just means input real, I'm not an apple-addict at all ;)
		real=iReal;
	}

	public BrewingStandBlock(boolean iReal, boolean iChest, Block iBlock)
	{
		// iReal just means input real, I'm not an apple-addict at all ;)
		real=iReal;
		chest=iChest;
		block=iBlock;
	}

	public boolean hasRepeaters()
	{
		for(int i=0;i<4;i++)
			if(repeaters[i]>0)
				return true;

		return false;
	}

	public BrewingStandBlock getReceiving()
	{
		return getLinkedBlock(1);
	}
	
	public BrewingStandBlock getSending()
	{
		return getLinkedBlock(2);
	}

	private BrewingStandBlock getLinkedBlock(int toSearch)
	{
		int index=-1;
		for(int i=0;i<4;i++)
		{
			if(repeaters[i]==toSearch)
			{
				index=i;
				break;
			}
		}

		if(index==-1)
			return new BrewingStandBlock(false);

		Block sender=block.getRelative(MiscFunc.idToFace(index), 2);
		if(sender.getType()!=Material.BREWING_STAND)
		{
			if(sender.getType()==Material.CHEST || sender.getType() == Material.TRAPPED_CHEST)
				return new BrewingStandBlock(false, true, sender);
			else
				return new BrewingStandBlock(false);
		}
		
		return makeBrewingBlock(sender);
	}

	public static BrewingStandBlock makeBrewingBlock(Block block)
	{
		BrewingStandBlock stand=new BrewingStandBlock();
		stand.block=block;
		for(int i=0;i<4;i++)
		{
			Block relativeBlock=block.getRelative(MiscFunc.idToFace(i)); 
			if(relativeBlock.getType() == Material.DIODE_BLOCK_OFF || relativeBlock.getType() == Material.DIODE_BLOCK_ON) // Is a diode
			{
				stand.repeaters[i]=diodeRole(MiscFunc.idToFace(i), relativeBlock.getData());
			}
		}
		return stand;
	}

	public static byte diodeRole(BlockFace originFace, byte data)
	{
		BlockFace facing=getDiodeFacing(data);

		if(facing==originFace)
			return 2;
		else if(facing==MiscFunc.opposedFace(originFace))
			return 1;
		return 0;
	}

	public static BlockFace getDiodeFacing(int diodeData)
	{
		int delayData = diodeData >> 2;
		int faceData = diodeData-(delayData<<2);

		while(faceData>3) // bug forces that
			faceData-=4;

		return MiscFunc.idToFace(faceData);
	}
}

