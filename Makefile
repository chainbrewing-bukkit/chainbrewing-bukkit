PKGPATH=fr/tobast/bukkit/chainbrewing
TARGET=chainbrewing

all: $(TARGET)

$(TARGET):
	@cd $(PKGPATH) && make
	jar cf $@.jar $(PKGPATH)/*.class plugin.yml LICENCE.txt

clean:
	@cd $(PKGPATH) && make clean


testing: $(TARGET)
	cp $<.jar ~/.minecrafttesting/plugins/


