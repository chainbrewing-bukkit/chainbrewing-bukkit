ChainBrewing - Bukkit plugin
----------------------------

	By Théophile "Tobast" BASTIAN.
	Distributed under the terms of the GNU GPLv3 licence, see LICENCE.txt file.

Homepage, description & manual: http://dev.bukkit.org/server-mods/chainbrewing/

Permissions:
	chainbrewing.chainbrew: allows a player to use a brewing chain.


